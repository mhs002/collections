from __future__ import absolute_import, division, print_function, unicode_literals

import numpy as np
from tensorflow import keras
import tensorflow as tf 
from mlxtend.data import loadlocal_mnist
import matplotlib.pyplot as plt

(training_features, training_labels), (testing_features, testing_labels) = keras.datasets.mnist.load_data()

training_features = training_features / 255.0
testing_features = testing_features / 255.0

NUMBERS = ["0","1","2","3","4","5","6","7","8","9"]

model = keras.Sequential([
        keras.layers.Flatten(input_shape=(28,28)),
        keras.layers.Dense(128,activation="relu"),
        keras.layers.Dense(128,activation="relu"),
        keras.layers.Dense(10,activation="softmax")
            
            ])

model.compile(
        optimizer = "adam",
        loss = "sparse_categorical_crossentropy",
        metrics = ["accuracy"]
        )

model.fit(training_features,training_labels,epochs=5)
test_loss , test_acc = model.evaluate(testing_features,testing_labels,verbose=1)
print("The test accuracy is, ",test_acc)

prediction = model.predict(testing_features)
maximum = np.argmax(prediction[1])
print("The number predicted is {}".format(NUMBERS[maximum]))

plt.figure()
plt.imshow(testing_features[1])
plt.colorbar()
plt.grid(False)
plt.show()
