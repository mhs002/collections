from __future__ import absolute_import, division, print_function, unicode_literals
import tensorflow as tf
import pandas as pd

CSV_COLUMN_NAMES = ['SepalLength', 'SepalWidth', 'PetalLength', 'PetalWidth', 'Species']
SPECIES = ['Setosa', 'Versicolor', 'Virginica']

train_path = tf.keras.utils.get_file(
    "iris_training.csv", "https://storage.googleapis.com/download.tensorflow.org/data/iris_training.csv")
test_path = tf.keras.utils.get_file(
    "iris_test.csv", "https://storage.googleapis.com/download.tensorflow.org/data/iris_test.csv")

df_train = pd.read_csv(train_path, names=CSV_COLUMN_NAMES, header=0)
df_test = pd.read_csv(test_path, names=CSV_COLUMN_NAMES, header=0)
y_train = df_train.pop("Species")
y_test = df_test.pop("Species")
feature_columns = []

for feature in df_train.keys():
    feature_columns.append(tf.feature_column.numeric_column(key=feature))

def input_func(features,labels,train=True,batch_size=256):
    ds = tf.data.Dataset.from_tensor_slices((dict(features),labels))
    if train:
        ds.shuffle(1000).repeat()
    return ds.batch(batch_size)

classificator = tf.estimator.DNNClassifier(
        feature_columns=feature_columns,
        hidden_units=[30,10],
        n_classes=3
        )

classificator.train(input_fn=lambda: input_func(df_train,y_train),steps=5000)
result = classificator.evaluate(input_fn=lambda: input_func(df_test,y_test,train=False))

# Override input function 
def input_func(features,batch_size=256):
    return tf.data.Dataset.from_tensor_slices(dict(features)).batch(batch_size)

CSV_COLUMN_NAMES_1 = ['SepalLength', 'SepalWidth', 'PetalLength', 'PetalWidth']
predict = {}

for name in CSV_COLUMN_NAMES_1:
    var = input(name+": ")
    predict[name] = [float(var)]

predict_obj = classificator.predict(input_fn=lambda: input_func(predict))

for name in predict_obj:
    class_id = name["class_ids"][0]
    probability = name["probabilities"][class_id]

print("The predicted outcome is: ",SPECIES[class_id]," with a probability of ",probability*100)
