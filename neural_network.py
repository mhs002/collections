import tensorflow as tf
from tensorflow import keras
import numpy as np 
import matplotlib.pyplot as plt

fashion_mnist = keras.datasets.fashion_mnist  # load dataset

(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data() 

train_images = train_images / 255.0
test_images = test_images / 255.0
class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
               'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

model = keras.Sequential(
        [
            keras.layers.Flatten(input_shape=(28,28)),
            keras.layers.Dense(128,activation="relu"),
            keras.layers.Dense(10,activation="softmax"),
        ]
        )
model.compile( 
        optimizer = "adam",
        loss = "sparse_categorical_crossentropy",
        metrics = ["accuracy"]
        )

model.fit(train_images,train_labels,epochs=5)
test_loss, test_acc = model.evaluate(test_images,test_labels,verbose=1)

prediction = model.predict(test_images)
print("The prediction is: {}".format(class_names[np.argmax(prediction[1])]))

plt.figure()
plt.imshow(test_images[1])
plt.colorbar()
plt.grid(False)
plt.show()
