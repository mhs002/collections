import tensorflow as tf
from tensorflow.keras import datasets, layers,models
from tensorflow import keras
import matplotlib.pyplot as plt
import numpy as np 


(train_images, train_labels) , (test_images, test_labels) = datasets.cifar10.load_data()

train_images , test_images = train_images / 255.0 , test_images / 255.0

class_names = [ 
        "airplane","automoblie","bird","cat","deer","dog","frog","horse","ship","truck"
        ]

model = keras.models.Sequential([
            keras.layers.Conv2D(32,kernel_size=(3,3),activation="relu",padding="valid"), # Padding can be same
            keras.layers.MaxPooling2D((2,2)),
            
            keras.layers.Conv2D(64,kernel_size=(3,3),activation="relu",padding="valid"),
            keras.layers.MaxPooling2D((2,2)),
            
            keras.layers.Conv2D(64,kernel_size=(3,3),activation="relu",padding="valid"),
            keras.layers.MaxPooling2D((2,2)),

            keras.layers.Flatten(input_shape=(32,32,3)),
            keras.layers.Dense(128,activation="relu"),
            keras.layers.Dense(10,activation="softmax")

        ])
model.compile(
        optimizer = "adam",
        loss = "sparse_categorical_crossentropy",
        metrics = ["accuracy"]
        )

model.fit(train_images,train_labels,epochs=7)
test_loss , test_acc = model.evaluate(test_images,test_labels,verbose=1)

print("The test accuracy is: {}".format(test_acc))

