from tensorflow import keras 
from tensorflow.keras import datasets, layers,models
from tensorflow.keras.preprocessing import image
from tensorflow.keras.preprocessing.image import ImageDataGenerator

import tensorflow as tf
import numpy as np 
import matplotlib.pyplot as plt 


(train_images , train_labels) , (test_images, test_labels) = datasets.cifar10.load_data()

train_images, test_images = train_images / 255.0 , test_images / 255.0 

datagen = ImageDataGenerator(
        rotation_range = 40,
        width_shift_range = 0.2,
        height_shift_range = 0.2,
        shear_range= 0.2,
        zoom_range = 0.2,
        horizontal_flip = True,
        fill_mode = 'nearest'
)

augmented_images = []

for i in range(5):
    image = np.expand_dims(train_images[i],axis=0)
    it = datagen.flow(image,batch_size=313) # Creates an image iterator that changes the image
    for i in range(9):
        batch = it.next() # Extracts a new image from the batch created
        augmented_images.append(batch[0]) # Stores the augmented images in a numpy array
aug_images = np.asarray(augmented_images) # Changes the list of images into numpy array

model = keras.models.Sequential([
        keras.layers.Conv2D(32,kernel_size=(3,3),activation="relu",padding="same"),
        keras.layers.MaxPooling2D((2,2)),
        
        keras.layers.Conv2D(64,kernel_size=(3,3),activation="relu",padding="same"),
        
        keras.layers.Conv2D(64,kernel_size=(3,3),activation="relu",padding="same"),
        keras.layers.MaxPooling2D((2,2)),

        keras.layers.Flatten(),
        keras.layers.Dense(128,activation="relu"),
        keras.layers.Dense(10,activation="softmax")
        ])

model.compile(
        optimizer= "adam",
        loss="sparse_categorical_crossentropy",
        metrics=["accuracy"]
        )

model.fit(aug_images,train_labels,epochs=10)
test_loss, test_acc = model.evaluate(test_images,test_labels)

print(test_acc)
